using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class CharacterMovement : MonoBehaviour
{
    #region Variables
    [Header("Movement")] 
    [SerializeField] private float moveSpeed;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    private PlayerInput playerInput;
    private CharacterController characterController;
    private Vector2 currentMovementInput;
    private Vector3 currentMovement;
    private Vector3 moveDirection;
    float movementDirectionY;

    [Header("Stamina")]
    [SerializeField] private float staminaAmount;
    [SerializeField] private StaminaBar staminaBar;
    [SerializeField] private float maxStamina;
    [SerializeField] private float sprintStaminaCost;
    [SerializeField] private float staminaRechargeAmount;

    [Header("Jump")]
    [SerializeField] private float jumpForce;
    private float gravity = 9.81f;
    private float groundedGravity = -.05f;

    [Header("Crouch")]
    [SerializeField] private float crouchSpeed;
    [SerializeField] private float crouchYScale;
    [SerializeField] private float startYScale;

    [Header("Camera")]
    [SerializeField] private Camera cam;
    private MouseLook mouseLook = new MouseLook();

    [Header("Guard Checks")]
    private bool isMovementPressed;
    private bool isSprintPressed;
    private bool isJumpPressed;
    private bool isCrouchPressed;
    private bool isIdle;
    private bool isWalking;
    private bool isJumping;
    private bool isSprinting;
    private bool isCrouching;
    #endregion

    private void Awake()
    {
        playerInput = new PlayerInput();
        characterController = GetComponent<CharacterController>();
        staminaBar.SetMaxStamina(maxStamina);
        staminaBar.HideUI();
        startYScale = transform.localScale.y;
        #region New Input System Assignments
        playerInput.CharacterControls.Move.started += OnMovement;
        playerInput.CharacterControls.Move.canceled += OnMovement;
        playerInput.CharacterControls.Move.performed += OnMovement;
        playerInput.CharacterControls.Sprint.started += OnSprint;
        playerInput.CharacterControls.Sprint.canceled += OnSprint;
        playerInput.CharacterControls.Jump.started += OnJump;
        playerInput.CharacterControls.Jump.canceled += OnJump;
        playerInput.CharacterControls.Crouch.started += OnCrouch;
        playerInput.CharacterControls.Crouch.canceled += OnCrouch;
        #endregion
        mouseLook.Init(transform, cam.transform);
    }

    #region New Input System Callback
    void OnMovement(InputAction.CallbackContext context)
    {
        currentMovementInput = context.ReadValue<Vector2>();
        currentMovement.x = currentMovementInput.x;
        currentMovement.y = currentMovementInput.y;
        isMovementPressed = currentMovement.x != 0 || currentMovement.y != 0;
    }

    void OnSprint(InputAction.CallbackContext context)
    {
        isSprintPressed = context.ReadValueAsButton();
    }

    void OnJump(InputAction.CallbackContext context)
    {
        isJumpPressed = context.ReadValueAsButton();
    }

    void OnCrouch(InputAction.CallbackContext context)
    {
        isCrouchPressed = context.ReadValueAsButton();
    }
    #endregion

    private void Update()
    {
        SpeedHandler();
        movementDirectionY = moveDirection.y;
        moveDirection = ((transform.right * currentMovement.x) + (transform.forward * currentMovement.y)) * moveSpeed;
        if (isCrouchPressed)
        {
            Crouch();
        }
        if (!isCrouchPressed)
        {
            transform.localScale = new Vector3(transform.localScale.x, startYScale, transform.localScale.z);
            Jump();
        }
        Gravity();
        PlayerRotation();
        staminaBar.SetStamina(staminaAmount);
        characterController.Move(moveDirection * Time.deltaTime);
    }

    void PlayerRotation()
    {
        mouseLook.LookRotation(transform, cam.transform);
    }

    void Crouch()
    {
        transform.localScale = new Vector3(transform.localScale.x, crouchYScale, transform.localScale.z);
    }

    void Gravity()
    {
        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }
    }

    void Jump()
    {
        if (isJumpPressed && characterController.isGrounded)
        {
            moveDirection.y = jumpForce;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }
    }

    void SpeedHandler()
    {
        if (isSprintPressed && !isCrouchPressed && staminaAmount > 0)
        {
            staminaBar.ShowUI();
            moveSpeed = runSpeed;
            staminaAmount -= sprintStaminaCost * Time.deltaTime;
        }
        else if (isCrouchPressed)
        {
            moveSpeed = crouchSpeed;
            RechargeStamina();
        }
        else
        {
            moveSpeed = walkSpeed;
            RechargeStamina();
        }
 
    }   

    void RechargeStamina()
    {
        if (staminaAmount == maxStamina)
        {
            staminaBar.HideUI();
            return;
        }
        staminaAmount += staminaRechargeAmount * Time.deltaTime;
        if (staminaAmount > maxStamina)
            staminaAmount = maxStamina;
    }

    #region Enable Disable Calls
    private void OnEnable()
    {
        playerInput.CharacterControls.Enable(); 
    }

    private void OnDisable()
    {
        playerInput.CharacterControls.Disable();
    }
    #endregion
}
